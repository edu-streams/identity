import * as React from "react";
import { Route, Routes } from "react-router-dom";
import { MainLayout } from "./layouts/MainLayout";
import IndexPage from "./pages/IndexPage";
import Callback from "./components/Callback";

export const CurrentUser = React.createContext({})

export default function App() {


    return (
          <Routes>
              <Route path="/identity">
                <Route element={<MainLayout />}>
                  <Route index element={<IndexPage />} />
                  <Route path="callback" element={<Callback />} />
                    {/*<Route path="debug" element={<DebugPage />} />*/}
                  {/*<Route path="*" element={<ErrorPage />} />*/}
                </Route>
              </Route>
          </Routes>
    );
}
