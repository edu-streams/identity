import * as React from "react";
import * as ReactDOM from "react-dom/client";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "@mui/material/styles";
import App from "./App";
import theme from "./theme";
import {BrowserRouter, HashRouter} from "react-router-dom";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import {LogtoProvider, LogtoConfig} from '@logto/react';
import { env } from 'env'

const rootElement = document.getElementById("root");
const root = ReactDOM.createRoot(rootElement);

const config: LogtoConfig = {
    endpoint: env.REACT_APP_LOGTO_ENDPOINT,
    appId: env.REACT_APP_LOGTO_APP_ID,
    resources: env.REACT_APP_LOGTO_RESOURCES,
    scopes: env.REACT_APP_LOGTO_SCOPES
};

root.render(
    <LogtoProvider config={config}>
        <ThemeProvider theme={theme}>
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </ThemeProvider>
    </LogtoProvider>
);
