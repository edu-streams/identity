export const env = { ...process.env, ...{
    "REACT_APP_LOGTO_ENDPOINT": "https://6pbvns.logto.app/",
    "REACT_APP_LOGTO_APP_ID": "asnhh0px905tmbmdwlqm1",
    "REACT_APP_LOGTO_RESOURCES": ['https://stage.edu-streams.org'],
    "REACT_APP_LOGTO_SCOPES": ['read:me'],
    "REACT_APP_LOGTO_URL_SIGNIN": "http://localhost:3000/identity/callback",
    "REACT_APP_LOGTO_URL_SIGNOUT": "http://localhost:3000"
},...window['env'] }