import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import AdbIcon from "@mui/icons-material/Adb";
import { Link } from "react-router-dom";
import {useLogto} from "@logto/react";
import { env } from 'env'
import {CurrentUser} from "../App";
import {useContext, useEffect} from "react";
import { useSearchParams } from 'react-router-dom';

const pages = ["Error", "Debug"];
const settings = ["Logout"];

function ResponsiveAppBar() {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const [user, setUser] = React.useState({});
  const { signIn, signOut, isAuthenticated, getAccessToken, fetchUserInfo, getIdTokenClaims} = useLogto();

    const [searchParams] = useSearchParams();

    useEffect(() => {
        const getUser = async () => {
            try {
                const res = await fetchUserInfo()
                setUser(res)
            } catch (e) {
                console.log(e);
            }
        };

        if(isAuthenticated){
            getUser();
        }
    }, [isAuthenticated]);

    useEffect(() => {
        let returnToNew = searchParams.get('returnTo');
        if (returnToNew) {
            returnToNew = window.location.href.replace(new RegExp(".*returnTo=", "gm"), "")
            localStorage.setItem("returnToNew", returnToNew)
            signIn(env.REACT_APP_LOGTO_URL_SIGNIN)
            // console.log(returnToNew, 333)
        }
    }, [])

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          {/*<AdbIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />*/}
          <Typography
            variant="h6"
            noWrap
            component={Link}
            to=""
            sx={{
              mr: 2,
              display: { xs: "none", md: "flex" },
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "inherit",
              textDecoration: "none",
            }}
          >
            Identity
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            {/*<IconButton*/}
            {/*  size="large"*/}
            {/*  aria-label="account of current user"*/}
            {/*  aria-controls="menu-appbar"*/}
            {/*  aria-haspopup="true"*/}
            {/*  onClick={handleOpenNavMenu}*/}
            {/*  color="inherit"*/}
            {/*>*/}
            {/*  <MenuIcon />*/}
            {/*</IconButton>*/}
            {/*<Menu*/}
            {/*  id="menu-appbar"*/}
            {/*  anchorEl={anchorElNav}*/}
            {/*  anchorOrigin={{*/}
            {/*    vertical: "bottom",*/}
            {/*    horizontal: "left",*/}
            {/*  }}*/}
            {/*  keepMounted*/}
            {/*  transformOrigin={{*/}
            {/*    vertical: "top",*/}
            {/*    horizontal: "left",*/}
            {/*  }}*/}
            {/*  open={Boolean(anchorElNav)}*/}
            {/*  onClose={handleCloseNavMenu}*/}
            {/*  sx={{*/}
            {/*    display: { xs: "block", md: "none" },*/}
            {/*  }}*/}
            {/*>*/}
            {/*  {pages.map((page) => (*/}
            {/*    <MenuItem key={page} onClick={handleCloseNavMenu}>*/}
            {/*      <Typography textAlign="center">{page}</Typography>*/}
            {/*    </MenuItem>*/}
            {/*  ))}*/}
            {/*</Menu>*/}
          </Box>
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            {/*{pages.map((page) => (*/}
            {/*  <Button*/}
            {/*    key={page}*/}
            {/*    component={Link}*/}
            {/*    to={page.toLowerCase()}*/}
            {/*    onClick={handleCloseNavMenu}*/}
            {/*    sx={{ my: 2, color: "white", display: "block" }}*/}
            {/*  >*/}
            {/*    {page}*/}
            {/*  </Button>*/}
            {/*))}*/}
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar alt="Remy Sharp" src={user?.picture} />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: "45px" }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
                {isAuthenticated ? (
                    <MenuItem onClick={() => signOut(env.REACT_APP_LOGTO_URL_SIGNOUT)}>
                        <Typography textAlign="center">SignOut</Typography>
                    </MenuItem>
                ) : (
                    <MenuItem onClick={() => signIn(env.REACT_APP_LOGTO_URL_SIGNIN)}>
                        <Typography textAlign="center">SignIn</Typography>
                    </MenuItem>
                )}
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default ResponsiveAppBar;
