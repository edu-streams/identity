import { useHandleSignInCallback } from '@logto/react';
import {Navigate, redirect} from "react-router-dom";
import {useEffect} from "react";
import {env} from "../env";

export default function Callback() {
    const { isLoading } = useHandleSignInCallback(() => {
        let returnToNew = localStorage.getItem("returnToNew")
        if (!(returnToNew === null)){
            localStorage.removeItem("returnToNew")
            window.location = returnToNew
        }

    });

    // When it's working in progress
    if (isLoading) {
        return <div>Redirecting...</div>;
    }

};