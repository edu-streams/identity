const SignOut = () => {
    const { signOut } = useLogto();

    return (
        <button onClick={() => signOut('http://localhost:3000/identity')}>
            Sign out
        </button>
    );
};